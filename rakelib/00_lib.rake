def require_program(p, pkg=nil)
  pkg ||= p
  if !system("which", p, out: "/dev/null")
    fail "Required program \`#{p}\` required, but not installed. Install \`#{pkg}\`."
  end
end
