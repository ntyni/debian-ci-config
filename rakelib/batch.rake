desc 'Converge master node, but only the debci master config'
task 'converge-debci-master' do
  Rake::Task['apply:' + $master_node.hostname].invoke("cookbooks/debci/master.rb")
end
