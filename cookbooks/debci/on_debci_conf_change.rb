define :on_debci_conf_change, action: :reload do
  hash = '/etc/debci/conf.hash'
  service_to_reload = params[:name]
  action = params[:action] || :reload
  execute 'hash config' do
    command   "mv #{hash}.new #{hash}"
    not_if    "find /etc/debci/conf.d/ -type f | sort | xargs sha1sum > #{hash}.new; cmp #{hash}.new #{hash}"
    notifies  action, "service[#{service_to_reload}]"
  end
end
