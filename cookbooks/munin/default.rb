package 'munin'

file '/etc/munin/munin.conf' do
  content "includedir /etc/munin/munin-conf.d\n"
  owner   'root'
  group   'root'
  mode    '0644'
end

template '/etc/munin/munin-conf.d/hosts.conf' do
  owner   'root'
  group   'root'
  mode    '0644'
end
