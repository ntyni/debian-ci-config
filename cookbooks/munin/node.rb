require 'socket'

package 'munin-node'

service 'munin-node' do
  action [:enable, :start]
end

execute "allow connections from munin master" do
  def master_ip
    @master_ip = node['hosts'].size == 1 ? 'localhost' : nil
    @master_ip ||= (node['munin'] && node['munin']['master_ip'])
    @master_ip ||=
      begin
        master_node = node['hosts'].keys.find { |h| h =~ /master/ }
        node['hosts'][master_node]
      end
  end
  command "echo 'cidr_allow #{master_ip}/32' >> /etc/munin/munin-node.conf"
  not_if "grep 'cidr_allow #{master_ip}/32' /etc/munin/munin-node.conf"
  notifies :restart, 'service[munin-node]'
end

execute "set munin-node hostname" do
  hostname = node['fqdn']
  command "sed -i -e '/^host_name\s/d; $a host_name #{hostname}' /etc/munin/munin-node.conf"
  not_if "grep '^host_name #{hostname}$' /etc/munin/munin-node.conf"
  notifies :restart, 'service[munin-node]'
end
